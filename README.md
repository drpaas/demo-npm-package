# Anpassungen

https://docs.gitlab.com/ee/user/packages/npm_registry/

instance level

`package.json`: `"name": "@my-org/package-name"` --> `"name": "@drpaas/demo-npm-package"`

- [X] [Publishing a package via command line](https://docs.gitlab.com/ee/user/packages/npm_registry/#publishing-a-package-via-the-command-line)
- [X] [Publishing a package via CI/CD pipeline] [](https://docs.gitlab.com/ee/user/packages/npm_registry/#publishing-a-package-via-a-cicd-pipeline)
- [X] [Install from the instance level](https://docs.gitlab.com/ee/user/packages/npm_registry/#install-from-the-instance-level)
- [ ] [Install npm packages from other organizations](https://docs.gitlab.com/ee/user/packages/npm_registry/#install-npm-packages-from-other-organizations)
- [ ] [npm metadata](https://docs.gitlab.com/ee/user/packages/npm_registry/#npm-metadata)
- [ ] [Add npm distribution tags](https://docs.gitlab.com/ee/user/packages/npm_registry/#add-npm-distribution-tags)
- [ ] [supported cli commands](https://docs.gitlab.com/ee/user/packages/npm_registry/#supported-cli-commands)

`.envrc`:

```
export NPM_TOKEN=...
export GITLAB_NPM_SERVER=gitlab.com
export GITLAB_PROJECT_ID=...
export GITLAB_NPM_SCOPE=drpaas
```

## Test package

Minimum package.json anlegen: `{ "name":"test"}`

.npmrc testen mit: `npm config ls`

package installieren: `npm i @drpaas/demo-npm-package`

### Node Express template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

### Developing with Gitpod

This template has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

If you open this project in Gitpod, you'll get all Node dependencies pre-installed and Express will open a web preview.
